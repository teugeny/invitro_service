<?php

namespace App\Entity;

/**
 * CallsLog
 */
class CallsLog
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $callSettingId;

    /**
     * @var string
     */
    private $event;

    /**
     * @var \DateTime|null
     */
    private $callStart;

    /**
     * @var string
     */
    private $pbxCallId;

    /**
     * @var string|null
     */
    private $internal;

    /**
     * @var string|null
     */
    private $destination;

    /**
     * @var string|null
     */
    private $duration;

    /**
     * @var string|null
     */
    private $disposition;

    /**
     * @var string|null
     */
    private $statusCode;

    /**
     * @var string|null
     */
    private $isRecorded;

    /**
     * @var string|null
     */
    private $callIdWithRec;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var string|null
     */
    private $callerId;

    /**
     * @var string|null
     */
    private $calledDid;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set callSettingId.
     *
     * @param int $callSettingId
     *
     * @return CallsLog
     */
    public function setCallSettingId($callSettingId)
    {
        $this->callSettingId = $callSettingId;

        return $this;
    }

    /**
     * Get callSettingId.
     *
     * @return int
     */
    public function getCallSettingId()
    {
        return $this->callSettingId;
    }

    /**
     * Set event.
     *
     * @param string $event
     *
     * @return CallsLog
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event.
     *
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set callStart.
     *
     * @param \DateTime|null $callStart
     *
     * @return CallsLog
     */
    public function setCallStart($callStart = null)
    {
        $this->callStart = $callStart;

        return $this;
    }

    /**
     * Get callStart.
     *
     * @return \DateTime|null
     */
    public function getCallStart()
    {
        return $this->callStart;
    }

    /**
     * Set pbxCallId.
     *
     * @param string $pbxCallId
     *
     * @return CallsLog
     */
    public function setPbxCallId($pbxCallId)
    {
        $this->pbxCallId = $pbxCallId;

        return $this;
    }

    /**
     * Get pbxCallId.
     *
     * @return string
     */
    public function getPbxCallId()
    {
        return $this->pbxCallId;
    }

    /**
     * Set internal.
     *
     * @param string|null $internal
     *
     * @return CallsLog
     */
    public function setInternal($internal = null)
    {
        $this->internal = $internal;

        return $this;
    }

    /**
     * Get internal.
     *
     * @return string|null
     */
    public function getInternal()
    {
        return $this->internal;
    }

    /**
     * Set destination.
     *
     * @param string|null $destination
     *
     * @return CallsLog
     */
    public function setDestination($destination = null)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination.
     *
     * @return string|null
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set duration.
     *
     * @param string|null $duration
     *
     * @return CallsLog
     */
    public function setDuration($duration = null)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration.
     *
     * @return string|null
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set disposition.
     *
     * @param string|null $disposition
     *
     * @return CallsLog
     */
    public function setDisposition($disposition = null)
    {
        $this->disposition = $disposition;

        return $this;
    }

    /**
     * Get disposition.
     *
     * @return string|null
     */
    public function getDisposition()
    {
        return $this->disposition;
    }

    /**
     * Set statusCode.
     *
     * @param string|null $statusCode
     *
     * @return CallsLog
     */
    public function setStatusCode($statusCode = null)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get statusCode.
     *
     * @return string|null
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set isRecorded.
     *
     * @param string|null $isRecorded
     *
     * @return CallsLog
     */
    public function setIsRecorded($isRecorded = null)
    {
        $this->isRecorded = $isRecorded;

        return $this;
    }

    /**
     * Get isRecorded.
     *
     * @return string|null
     */
    public function getIsRecorded()
    {
        return $this->isRecorded;
    }

    /**
     * Set callIdWithRec.
     *
     * @param string|null $callIdWithRec
     *
     * @return CallsLog
     */
    public function setCallIdWithRec($callIdWithRec = null)
    {
        $this->callIdWithRec = $callIdWithRec;

        return $this;
    }

    /**
     * Get callIdWithRec.
     *
     * @return string|null
     */
    public function getCallIdWithRec()
    {
        return $this->callIdWithRec;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return CallsLog
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return CallsLog
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return CallsLog
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set callerId.
     *
     * @param string|null $callerId
     *
     * @return CallsLog
     */
    public function setCallerId($callerId = null)
    {
        $this->callerId = $callerId;

        return $this;
    }

    /**
     * Get callerId.
     *
     * @return string|null
     */
    public function getCallerId()
    {
        return $this->callerId;
    }

    /**
     * Set calledDid.
     *
     * @param string|null $calledDid
     *
     * @return CallsLog
     */
    public function setCalledDid($calledDid = null)
    {
        $this->calledDid = $calledDid;

        return $this;
    }

    /**
     * Get calledDid.
     *
     * @return string|null
     */
    public function getCalledDid()
    {
        return $this->calledDid;
    }
}
