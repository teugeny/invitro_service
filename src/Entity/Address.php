<?php

namespace App\Entity;

/**
 * Address
 */
class Address
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var int|null
     */
    private $house;

    /**
     * @var int|null
     */
    private $apartment;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var string
     */
    private $street;

    /**
     * @var \App\Entity\States
     */
    private $city;

    /**
     * @var \App\Entity\Countries
     */
    private $country;

    /**
     * @var \App\Entity\States
     */
    private $state;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set zip.
     *
     * @param string $zip
     *
     * @return Address
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip.
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set house.
     *
     * @param int|null $house
     *
     * @return Address
     */
    public function setHouse($house = null)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get house.
     *
     * @return int|null
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set apartment.
     *
     * @param int|null $apartment
     *
     * @return Address
     */
    public function setApartment($apartment = null)
    {
        $this->apartment = $apartment;

        return $this;
    }

    /**
     * Get apartment.
     *
     * @return int|null
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Address
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Address
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return Address
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set city.
     *
     * @param \App\Entity\States|null $city
     *
     * @return Address
     */
    public function setCity(\App\Entity\States $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return \App\Entity\States|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country.
     *
     * @param \App\Entity\Countries|null $country
     *
     * @return Address
     */
    public function setCountry(\App\Entity\Countries $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \App\Entity\Countries|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set state.
     *
     * @param \App\Entity\States|null $state
     *
     * @return Address
     */
    public function setState(\App\Entity\States $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return \App\Entity\States|null
     */
    public function getState()
    {
        return $this->state;
    }
}
