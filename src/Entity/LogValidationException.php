<?php

namespace App\Entity;

/**
 * LogValidationException
 */
class LogValidationException
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $errorsList;

    /**
     * @var string|null
     */
    private $requestData;

    /**
     * @var string
     */
    private $trace = '';

    /**
     * @var string|null
     */
    private $cookies;

    /**
     * @var string|null
     */
    private $session;

    /**
     * @var string|null
     */
    private $userPhone;

    /**
     * @var string|null
     */
    private $userAgent;

    /**
     * @var string|null
     */
    private $ip;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var bool
     */
    private $read = false;

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var string|null
     */
    private $pageName;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set errorsList.
     *
     * @param string $errorsList
     *
     * @return LogValidationException
     */
    public function setErrorsList($errorsList)
    {
        $this->errorsList = $errorsList;

        return $this;
    }

    /**
     * Get errorsList.
     *
     * @return string
     */
    public function getErrorsList()
    {
        return $this->errorsList;
    }

    /**
     * Set requestData.
     *
     * @param string|null $requestData
     *
     * @return LogValidationException
     */
    public function setRequestData($requestData = null)
    {
        $this->requestData = $requestData;

        return $this;
    }

    /**
     * Get requestData.
     *
     * @return string|null
     */
    public function getRequestData()
    {
        return $this->requestData;
    }

    /**
     * Set trace.
     *
     * @param string $trace
     *
     * @return LogValidationException
     */
    public function setTrace($trace)
    {
        $this->trace = $trace;

        return $this;
    }

    /**
     * Get trace.
     *
     * @return string
     */
    public function getTrace()
    {
        return $this->trace;
    }

    /**
     * Set cookies.
     *
     * @param string|null $cookies
     *
     * @return LogValidationException
     */
    public function setCookies($cookies = null)
    {
        $this->cookies = $cookies;

        return $this;
    }

    /**
     * Get cookies.
     *
     * @return string|null
     */
    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * Set session.
     *
     * @param string|null $session
     *
     * @return LogValidationException
     */
    public function setSession($session = null)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session.
     *
     * @return string|null
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set userPhone.
     *
     * @param string|null $userPhone
     *
     * @return LogValidationException
     */
    public function setUserPhone($userPhone = null)
    {
        $this->userPhone = $userPhone;

        return $this;
    }

    /**
     * Get userPhone.
     *
     * @return string|null
     */
    public function getUserPhone()
    {
        return $this->userPhone;
    }

    /**
     * Set userAgent.
     *
     * @param string|null $userAgent
     *
     * @return LogValidationException
     */
    public function setUserAgent($userAgent = null)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Get userAgent.
     *
     * @return string|null
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Set ip.
     *
     * @param string|null $ip
     *
     * @return LogValidationException
     */
    public function setIp($ip = null)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return LogValidationException
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set read.
     *
     * @param bool $read
     *
     * @return LogValidationException
     */
    public function setRead($read)
    {
        $this->read = $read;

        return $this;
    }

    /**
     * Get read.
     *
     * @return bool
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * Set url.
     *
     * @param string|null $url
     *
     * @return LogValidationException
     */
    public function setUrl($url = null)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set pageName.
     *
     * @param string|null $pageName
     *
     * @return LogValidationException
     */
    public function setPageName($pageName = null)
    {
        $this->pageName = $pageName;

        return $this;
    }

    /**
     * Get pageName.
     *
     * @return string|null
     */
    public function getPageName()
    {
        return $this->pageName;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return LogValidationException
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return LogValidationException
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return LogValidationException
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
