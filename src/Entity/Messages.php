<?php

namespace App\Entity;

/**
 * Messages
 */
class Messages
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $messageTypeId;

    /**
     * @var int
     */
    private $roomId;

    /**
     * @var int
     */
    private $fromUserId;

    /**
     * @var int
     */
    private $toUserId;

    /**
     * @var string|null
     */
    private $message;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var string
     */
    private $uuid;

    /**
     * @var bool
     */
    private $private = false;

    /**
     * @var bool
     */
    private $read = false;

    /**
     * @var \DateTime|null
     */
    private $readAt;

    /**
     * @var bool
     */
    private $notify = false;

    /**
     * @var \DateTime|null
     */
    private $notifyAt;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set messageTypeId.
     *
     * @param int $messageTypeId
     *
     * @return Messages
     */
    public function setMessageTypeId($messageTypeId)
    {
        $this->messageTypeId = $messageTypeId;

        return $this;
    }

    /**
     * Get messageTypeId.
     *
     * @return int
     */
    public function getMessageTypeId()
    {
        return $this->messageTypeId;
    }

    /**
     * Set roomId.
     *
     * @param int $roomId
     *
     * @return Messages
     */
    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;

        return $this;
    }

    /**
     * Get roomId.
     *
     * @return int
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Set fromUserId.
     *
     * @param int $fromUserId
     *
     * @return Messages
     */
    public function setFromUserId($fromUserId)
    {
        $this->fromUserId = $fromUserId;

        return $this;
    }

    /**
     * Get fromUserId.
     *
     * @return int
     */
    public function getFromUserId()
    {
        return $this->fromUserId;
    }

    /**
     * Set toUserId.
     *
     * @param int $toUserId
     *
     * @return Messages
     */
    public function setToUserId($toUserId)
    {
        $this->toUserId = $toUserId;

        return $this;
    }

    /**
     * Get toUserId.
     *
     * @return int
     */
    public function getToUserId()
    {
        return $this->toUserId;
    }

    /**
     * Set message.
     *
     * @param string|null $message
     *
     * @return Messages
     */
    public function setMessage($message = null)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message.
     *
     * @return string|null
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Messages
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Messages
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set uuid.
     *
     * @param string $uuid
     *
     * @return Messages
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid.
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set private.
     *
     * @param bool $private
     *
     * @return Messages
     */
    public function setPrivate($private)
    {
        $this->private = $private;

        return $this;
    }

    /**
     * Get private.
     *
     * @return bool
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * Set read.
     *
     * @param bool $read
     *
     * @return Messages
     */
    public function setRead($read)
    {
        $this->read = $read;

        return $this;
    }

    /**
     * Get read.
     *
     * @return bool
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * Set readAt.
     *
     * @param \DateTime|null $readAt
     *
     * @return Messages
     */
    public function setReadAt($readAt = null)
    {
        $this->readAt = $readAt;

        return $this;
    }

    /**
     * Get readAt.
     *
     * @return \DateTime|null
     */
    public function getReadAt()
    {
        return $this->readAt;
    }

    /**
     * Set notify.
     *
     * @param bool $notify
     *
     * @return Messages
     */
    public function setNotify($notify)
    {
        $this->notify = $notify;

        return $this;
    }

    /**
     * Get notify.
     *
     * @return bool
     */
    public function getNotify()
    {
        return $this->notify;
    }

    /**
     * Set notifyAt.
     *
     * @param \DateTime|null $notifyAt
     *
     * @return Messages
     */
    public function setNotifyAt($notifyAt = null)
    {
        $this->notifyAt = $notifyAt;

        return $this;
    }

    /**
     * Get notifyAt.
     *
     * @return \DateTime|null
     */
    public function getNotifyAt()
    {
        return $this->notifyAt;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return Messages
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
