<?php

namespace App\Entity;

/**
 * Jobs
 */
class Jobs
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $queue;

    /**
     * @var string
     */
    private $payload;

    /**
     * @var int
     */
    private $attempts;

    /**
     * @var int|null
     */
    private $reservedAt;

    /**
     * @var int
     */
    private $availableAt;

    /**
     * @var int
     */
    private $createdAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set queue.
     *
     * @param string $queue
     *
     * @return Jobs
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;

        return $this;
    }

    /**
     * Get queue.
     *
     * @return string
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * Set payload.
     *
     * @param string $payload
     *
     * @return Jobs
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;

        return $this;
    }

    /**
     * Get payload.
     *
     * @return string
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * Set attempts.
     *
     * @param int $attempts
     *
     * @return Jobs
     */
    public function setAttempts($attempts)
    {
        $this->attempts = $attempts;

        return $this;
    }

    /**
     * Get attempts.
     *
     * @return int
     */
    public function getAttempts()
    {
        return $this->attempts;
    }

    /**
     * Set reservedAt.
     *
     * @param int|null $reservedAt
     *
     * @return Jobs
     */
    public function setReservedAt($reservedAt = null)
    {
        $this->reservedAt = $reservedAt;

        return $this;
    }

    /**
     * Get reservedAt.
     *
     * @return int|null
     */
    public function getReservedAt()
    {
        return $this->reservedAt;
    }

    /**
     * Set availableAt.
     *
     * @param int $availableAt
     *
     * @return Jobs
     */
    public function setAvailableAt($availableAt)
    {
        $this->availableAt = $availableAt;

        return $this;
    }

    /**
     * Get availableAt.
     *
     * @return int
     */
    public function getAvailableAt()
    {
        return $this->availableAt;
    }

    /**
     * Set createdAt.
     *
     * @param int $createdAt
     *
     * @return Jobs
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
