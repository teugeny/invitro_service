<?php

namespace App\Entity;

/**
 * Notifications
 */
class Notifications
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $notifiableId;

    /**
     * @var string
     */
    private $notifiableType;

    /**
     * @var string
     */
    private $data;

    /**
     * @var \DateTime|null
     */
    private $readAt;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Notifications
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set notifiableId.
     *
     * @param int $notifiableId
     *
     * @return Notifications
     */
    public function setNotifiableId($notifiableId)
    {
        $this->notifiableId = $notifiableId;

        return $this;
    }

    /**
     * Get notifiableId.
     *
     * @return int
     */
    public function getNotifiableId()
    {
        return $this->notifiableId;
    }

    /**
     * Set notifiableType.
     *
     * @param string $notifiableType
     *
     * @return Notifications
     */
    public function setNotifiableType($notifiableType)
    {
        $this->notifiableType = $notifiableType;

        return $this;
    }

    /**
     * Get notifiableType.
     *
     * @return string
     */
    public function getNotifiableType()
    {
        return $this->notifiableType;
    }

    /**
     * Set data.
     *
     * @param string $data
     *
     * @return Notifications
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set readAt.
     *
     * @param \DateTime|null $readAt
     *
     * @return Notifications
     */
    public function setReadAt($readAt = null)
    {
        $this->readAt = $readAt;

        return $this;
    }

    /**
     * Get readAt.
     *
     * @return \DateTime|null
     */
    public function getReadAt()
    {
        return $this->readAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Notifications
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Notifications
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
