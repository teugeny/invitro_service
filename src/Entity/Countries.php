<?php

namespace App\Entity;

/**
 * Countries
 */
class Countries
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $active = true;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var int
     */
    private $pos = '0';

    /**
     * @var string|null
     */
    private $isoCode2d;

    /**
     * @var string|null
     */
    private $isoCode3d;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Countries
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Countries
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Countries
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Countries
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return Countries
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set pos.
     *
     * @param int $pos
     *
     * @return Countries
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos.
     *
     * @return int
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set isoCode2d.
     *
     * @param string|null $isoCode2d
     *
     * @return Countries
     */
    public function setIsoCode2d($isoCode2d = null)
    {
        $this->isoCode2d = $isoCode2d;

        return $this;
    }

    /**
     * Get isoCode2d.
     *
     * @return string|null
     */
    public function getIsoCode2d()
    {
        return $this->isoCode2d;
    }

    /**
     * Set isoCode3d.
     *
     * @param string|null $isoCode3d
     *
     * @return Countries
     */
    public function setIsoCode3d($isoCode3d = null)
    {
        $this->isoCode3d = $isoCode3d;

        return $this;
    }

    /**
     * Get isoCode3d.
     *
     * @return string|null
     */
    public function getIsoCode3d()
    {
        return $this->isoCode3d;
    }
}
