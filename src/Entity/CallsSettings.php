<?php

namespace App\Entity;

/**
 * CallsSettings
 */
class CallsSettings
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $atcDomain;

    /**
     * @var string|null
     */
    private $apiPath;

    /**
     * @var string|null
     */
    private $internalNumber;

    /**
     * @var string|null
     */
    private $internalName;

    /**
     * @var string|null
     */
    private $callerId;

    /**
     * @var string|null
     */
    private $atcPassword;

    /**
     * @var string|null
     */
    private $internalNumberOnAtc;

    /**
     * @var string|null
     */
    private $ip;

    /**
     * @var string|null
     */
    private $port;

    /**
     * @var string|null
     */
    private $userAgent;

    /**
     * @var string|null
     */
    private $apiKey;

    /**
     * @var string|null
     */
    private $apiSecret;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set atcDomain.
     *
     * @param string|null $atcDomain
     *
     * @return CallsSettings
     */
    public function setAtcDomain($atcDomain = null)
    {
        $this->atcDomain = $atcDomain;

        return $this;
    }

    /**
     * Get atcDomain.
     *
     * @return string|null
     */
    public function getAtcDomain()
    {
        return $this->atcDomain;
    }

    /**
     * Set apiPath.
     *
     * @param string|null $apiPath
     *
     * @return CallsSettings
     */
    public function setApiPath($apiPath = null)
    {
        $this->apiPath = $apiPath;

        return $this;
    }

    /**
     * Get apiPath.
     *
     * @return string|null
     */
    public function getApiPath()
    {
        return $this->apiPath;
    }

    /**
     * Set internalNumber.
     *
     * @param string|null $internalNumber
     *
     * @return CallsSettings
     */
    public function setInternalNumber($internalNumber = null)
    {
        $this->internalNumber = $internalNumber;

        return $this;
    }

    /**
     * Get internalNumber.
     *
     * @return string|null
     */
    public function getInternalNumber()
    {
        return $this->internalNumber;
    }

    /**
     * Set internalName.
     *
     * @param string|null $internalName
     *
     * @return CallsSettings
     */
    public function setInternalName($internalName = null)
    {
        $this->internalName = $internalName;

        return $this;
    }

    /**
     * Get internalName.
     *
     * @return string|null
     */
    public function getInternalName()
    {
        return $this->internalName;
    }

    /**
     * Set callerId.
     *
     * @param string|null $callerId
     *
     * @return CallsSettings
     */
    public function setCallerId($callerId = null)
    {
        $this->callerId = $callerId;

        return $this;
    }

    /**
     * Get callerId.
     *
     * @return string|null
     */
    public function getCallerId()
    {
        return $this->callerId;
    }

    /**
     * Set atcPassword.
     *
     * @param string|null $atcPassword
     *
     * @return CallsSettings
     */
    public function setAtcPassword($atcPassword = null)
    {
        $this->atcPassword = $atcPassword;

        return $this;
    }

    /**
     * Get atcPassword.
     *
     * @return string|null
     */
    public function getAtcPassword()
    {
        return $this->atcPassword;
    }

    /**
     * Set internalNumberOnAtc.
     *
     * @param string|null $internalNumberOnAtc
     *
     * @return CallsSettings
     */
    public function setInternalNumberOnAtc($internalNumberOnAtc = null)
    {
        $this->internalNumberOnAtc = $internalNumberOnAtc;

        return $this;
    }

    /**
     * Get internalNumberOnAtc.
     *
     * @return string|null
     */
    public function getInternalNumberOnAtc()
    {
        return $this->internalNumberOnAtc;
    }

    /**
     * Set ip.
     *
     * @param string|null $ip
     *
     * @return CallsSettings
     */
    public function setIp($ip = null)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set port.
     *
     * @param string|null $port
     *
     * @return CallsSettings
     */
    public function setPort($port = null)
    {
        $this->port = $port;

        return $this;
    }

    /**
     * Get port.
     *
     * @return string|null
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set userAgent.
     *
     * @param string|null $userAgent
     *
     * @return CallsSettings
     */
    public function setUserAgent($userAgent = null)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Get userAgent.
     *
     * @return string|null
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Set apiKey.
     *
     * @param string|null $apiKey
     *
     * @return CallsSettings
     */
    public function setApiKey($apiKey = null)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey.
     *
     * @return string|null
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set apiSecret.
     *
     * @param string|null $apiSecret
     *
     * @return CallsSettings
     */
    public function setApiSecret($apiSecret = null)
    {
        $this->apiSecret = $apiSecret;

        return $this;
    }

    /**
     * Get apiSecret.
     *
     * @return string|null
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return CallsSettings
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return CallsSettings
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
