<?php

namespace App\Entity;

/**
 * PasswordConfirmation
 */
class PasswordConfirmation
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $code;

    /**
     * @var bool
     */
    private $used;

    /**
     * @var \DateTime|null
     */
    private $usedAt;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone.
     *
     * @param string $phone
     *
     * @return PasswordConfirmation
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return PasswordConfirmation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set used.
     *
     * @param bool $used
     *
     * @return PasswordConfirmation
     */
    public function setUsed($used)
    {
        $this->used = $used;

        return $this;
    }

    /**
     * Get used.
     *
     * @return bool
     */
    public function getUsed()
    {
        return $this->used;
    }

    /**
     * Set usedAt.
     *
     * @param \DateTime|null $usedAt
     *
     * @return PasswordConfirmation
     */
    public function setUsedAt($usedAt = null)
    {
        $this->usedAt = $usedAt;

        return $this;
    }

    /**
     * Get usedAt.
     *
     * @return \DateTime|null
     */
    public function getUsedAt()
    {
        return $this->usedAt;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return PasswordConfirmation
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return PasswordConfirmation
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return PasswordConfirmation
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
