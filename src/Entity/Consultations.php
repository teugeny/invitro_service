<?php

namespace App\Entity;

/**
 * Consultations
 */
class Consultations
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $doctorId;

    /**
     * @var int
     */
    private $patientId;

    /**
     * @var bool
     */
    private $confirmed = false;

    /**
     * @var \DateTime|null
     */
    private $confirmedAt;

    /**
     * @var \DateTime|null
     */
    private $date;

    /**
     * @var \DateTime|null
     */
    private $time;

    /**
     * @var bool
     */
    private $success = false;

    /**
     * @var \DateTime|null
     */
    private $successAt;

    /**
     * @var bool
     */
    private $payed;

    /**
     * @var int
     */
    private $payedId;

    /**
     * @var \DateTime
     */
    private $payedAt;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var int|null
     */
    private $documentId;

    /**
     * @var string
     */
    private $type = 'online';

    /**
     * @var bool
     */
    private $cancel = false;

    /**
     * @var \DateTime|null
     */
    private $cancelAt;

    /**
     * @var \DateTime|null
     */
    private $nextConsultationAt;

    /**
     * @var string|null
     */
    private $recommendations;

    /**
     * @var int
     */
    private $price = '0';

    /**
     * @var int|null
     */
    private $serviceProductId;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set doctorId.
     *
     * @param int $doctorId
     *
     * @return Consultations
     */
    public function setDoctorId($doctorId)
    {
        $this->doctorId = $doctorId;

        return $this;
    }

    /**
     * Get doctorId.
     *
     * @return int
     */
    public function getDoctorId()
    {
        return $this->doctorId;
    }

    /**
     * Set patientId.
     *
     * @param int $patientId
     *
     * @return Consultations
     */
    public function setPatientId($patientId)
    {
        $this->patientId = $patientId;

        return $this;
    }

    /**
     * Get patientId.
     *
     * @return int
     */
    public function getPatientId()
    {
        return $this->patientId;
    }

    /**
     * Set confirmed.
     *
     * @param bool $confirmed
     *
     * @return Consultations
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * Get confirmed.
     *
     * @return bool
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * Set confirmedAt.
     *
     * @param \DateTime|null $confirmedAt
     *
     * @return Consultations
     */
    public function setConfirmedAt($confirmedAt = null)
    {
        $this->confirmedAt = $confirmedAt;

        return $this;
    }

    /**
     * Get confirmedAt.
     *
     * @return \DateTime|null
     */
    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    /**
     * Set date.
     *
     * @param \DateTime|null $date
     *
     * @return Consultations
     */
    public function setDate($date = null)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time.
     *
     * @param \DateTime|null $time
     *
     * @return Consultations
     */
    public function setTime($time = null)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time.
     *
     * @return \DateTime|null
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set success.
     *
     * @param bool $success
     *
     * @return Consultations
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get success.
     *
     * @return bool
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * Set successAt.
     *
     * @param \DateTime|null $successAt
     *
     * @return Consultations
     */
    public function setSuccessAt($successAt = null)
    {
        $this->successAt = $successAt;

        return $this;
    }

    /**
     * Get successAt.
     *
     * @return \DateTime|null
     */
    public function getSuccessAt()
    {
        return $this->successAt;
    }

    /**
     * Set payed.
     *
     * @param bool $payed
     *
     * @return Consultations
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;

        return $this;
    }

    /**
     * Get payed.
     *
     * @return bool
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * Set payedId.
     *
     * @param int $payedId
     *
     * @return Consultations
     */
    public function setPayedId($payedId)
    {
        $this->payedId = $payedId;

        return $this;
    }

    /**
     * Get payedId.
     *
     * @return int
     */
    public function getPayedId()
    {
        return $this->payedId;
    }

    /**
     * Set payedAt.
     *
     * @param \DateTime $payedAt
     *
     * @return Consultations
     */
    public function setPayedAt($payedAt)
    {
        $this->payedAt = $payedAt;

        return $this;
    }

    /**
     * Get payedAt.
     *
     * @return \DateTime
     */
    public function getPayedAt()
    {
        return $this->payedAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Consultations
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Consultations
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set documentId.
     *
     * @param int|null $documentId
     *
     * @return Consultations
     */
    public function setDocumentId($documentId = null)
    {
        $this->documentId = $documentId;

        return $this;
    }

    /**
     * Get documentId.
     *
     * @return int|null
     */
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Consultations
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set cancel.
     *
     * @param bool $cancel
     *
     * @return Consultations
     */
    public function setCancel($cancel)
    {
        $this->cancel = $cancel;

        return $this;
    }

    /**
     * Get cancel.
     *
     * @return bool
     */
    public function getCancel()
    {
        return $this->cancel;
    }

    /**
     * Set cancelAt.
     *
     * @param \DateTime|null $cancelAt
     *
     * @return Consultations
     */
    public function setCancelAt($cancelAt = null)
    {
        $this->cancelAt = $cancelAt;

        return $this;
    }

    /**
     * Get cancelAt.
     *
     * @return \DateTime|null
     */
    public function getCancelAt()
    {
        return $this->cancelAt;
    }

    /**
     * Set nextConsultationAt.
     *
     * @param \DateTime|null $nextConsultationAt
     *
     * @return Consultations
     */
    public function setNextConsultationAt($nextConsultationAt = null)
    {
        $this->nextConsultationAt = $nextConsultationAt;

        return $this;
    }

    /**
     * Get nextConsultationAt.
     *
     * @return \DateTime|null
     */
    public function getNextConsultationAt()
    {
        return $this->nextConsultationAt;
    }

    /**
     * Set recommendations.
     *
     * @param string|null $recommendations
     *
     * @return Consultations
     */
    public function setRecommendations($recommendations = null)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations.
     *
     * @return string|null
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

    /**
     * Set price.
     *
     * @param int $price
     *
     * @return Consultations
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set serviceProductId.
     *
     * @param int|null $serviceProductId
     *
     * @return Consultations
     */
    public function setServiceProductId($serviceProductId = null)
    {
        $this->serviceProductId = $serviceProductId;

        return $this;
    }

    /**
     * Get serviceProductId.
     *
     * @return int|null
     */
    public function getServiceProductId()
    {
        return $this->serviceProductId;
    }
}
