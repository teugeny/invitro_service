<?php

namespace App\Entity;

/**
 * Feedback
 */
class Feedback
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string|null
     */
    private $drugs;

    /**
     * @var string|null
     */
    private $allergy;

    /**
     * @var string|null
     */
    private $state;

    /**
     * @var string|null
     */
    private $target;

    /**
     * @var int|null
     */
    private $documentId;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var string|null
     */
    private $dayCycle;

    /**
     * @var string|null
     */
    private $changedrugs;

    /**
     * @var string|null
     */
    private $complaints;

    /**
     * @var \DateTime
     */
    private $date = '2018-12-03';

    /**
     * @var string|null
     */
    private $declinedrugs;

    /**
     * @var string|null
     */
    private $positivechanges;

    /**
     * @var string|null
     */
    private $vitamins;

    /**
     * @var float
     */
    private $weightbefore = '0';

    /**
     * @var float
     */
    private $weightnow = '0';


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return Feedback
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set drugs.
     *
     * @param string|null $drugs
     *
     * @return Feedback
     */
    public function setDrugs($drugs = null)
    {
        $this->drugs = $drugs;

        return $this;
    }

    /**
     * Get drugs.
     *
     * @return string|null
     */
    public function getDrugs()
    {
        return $this->drugs;
    }

    /**
     * Set allergy.
     *
     * @param string|null $allergy
     *
     * @return Feedback
     */
    public function setAllergy($allergy = null)
    {
        $this->allergy = $allergy;

        return $this;
    }

    /**
     * Get allergy.
     *
     * @return string|null
     */
    public function getAllergy()
    {
        return $this->allergy;
    }

    /**
     * Set state.
     *
     * @param string|null $state
     *
     * @return Feedback
     */
    public function setState($state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return string|null
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set target.
     *
     * @param string|null $target
     *
     * @return Feedback
     */
    public function setTarget($target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target.
     *
     * @return string|null
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set documentId.
     *
     * @param int|null $documentId
     *
     * @return Feedback
     */
    public function setDocumentId($documentId = null)
    {
        $this->documentId = $documentId;

        return $this;
    }

    /**
     * Get documentId.
     *
     * @return int|null
     */
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Feedback
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Feedback
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set dayCycle.
     *
     * @param string|null $dayCycle
     *
     * @return Feedback
     */
    public function setDayCycle($dayCycle = null)
    {
        $this->dayCycle = $dayCycle;

        return $this;
    }

    /**
     * Get dayCycle.
     *
     * @return string|null
     */
    public function getDayCycle()
    {
        return $this->dayCycle;
    }

    /**
     * Set changedrugs.
     *
     * @param string|null $changedrugs
     *
     * @return Feedback
     */
    public function setChangedrugs($changedrugs = null)
    {
        $this->changedrugs = $changedrugs;

        return $this;
    }

    /**
     * Get changedrugs.
     *
     * @return string|null
     */
    public function getChangedrugs()
    {
        return $this->changedrugs;
    }

    /**
     * Set complaints.
     *
     * @param string|null $complaints
     *
     * @return Feedback
     */
    public function setComplaints($complaints = null)
    {
        $this->complaints = $complaints;

        return $this;
    }

    /**
     * Get complaints.
     *
     * @return string|null
     */
    public function getComplaints()
    {
        return $this->complaints;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Feedback
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set declinedrugs.
     *
     * @param string|null $declinedrugs
     *
     * @return Feedback
     */
    public function setDeclinedrugs($declinedrugs = null)
    {
        $this->declinedrugs = $declinedrugs;

        return $this;
    }

    /**
     * Get declinedrugs.
     *
     * @return string|null
     */
    public function getDeclinedrugs()
    {
        return $this->declinedrugs;
    }

    /**
     * Set positivechanges.
     *
     * @param string|null $positivechanges
     *
     * @return Feedback
     */
    public function setPositivechanges($positivechanges = null)
    {
        $this->positivechanges = $positivechanges;

        return $this;
    }

    /**
     * Get positivechanges.
     *
     * @return string|null
     */
    public function getPositivechanges()
    {
        return $this->positivechanges;
    }

    /**
     * Set vitamins.
     *
     * @param string|null $vitamins
     *
     * @return Feedback
     */
    public function setVitamins($vitamins = null)
    {
        $this->vitamins = $vitamins;

        return $this;
    }

    /**
     * Get vitamins.
     *
     * @return string|null
     */
    public function getVitamins()
    {
        return $this->vitamins;
    }

    /**
     * Set weightbefore.
     *
     * @param float $weightbefore
     *
     * @return Feedback
     */
    public function setWeightbefore($weightbefore)
    {
        $this->weightbefore = $weightbefore;

        return $this;
    }

    /**
     * Get weightbefore.
     *
     * @return float
     */
    public function getWeightbefore()
    {
        return $this->weightbefore;
    }

    /**
     * Set weightnow.
     *
     * @param float $weightnow
     *
     * @return Feedback
     */
    public function setWeightnow($weightnow)
    {
        $this->weightnow = $weightnow;

        return $this;
    }

    /**
     * Get weightnow.
     *
     * @return float
     */
    public function getWeightnow()
    {
        return $this->weightnow;
    }
}
