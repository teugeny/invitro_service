<?php

namespace App\Entity;

/**
 * LogSms
 */
class LogSms
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var bool
     */
    private $read = false;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $text;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var int
     */
    private $type = '0';

    /**
     * @var string
     */
    private $gatewayId = '';

    /**
     * @var string
     */
    private $gatewayStatusId = '';

    /**
     * @var string
     */
    private $gatewayStatusText = '';

    /**
     * @var \DateTime|null
     */
    private $gatewayStatusUpdatedAt;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set read.
     *
     * @param bool $read
     *
     * @return LogSms
     */
    public function setRead($read)
    {
        $this->read = $read;

        return $this;
    }

    /**
     * Get read.
     *
     * @return bool
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * Set phone.
     *
     * @param string $phone
     *
     * @return LogSms
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return LogSms
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return LogSms
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set type.
     *
     * @param int $type
     *
     * @return LogSms
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set gatewayId.
     *
     * @param string $gatewayId
     *
     * @return LogSms
     */
    public function setGatewayId($gatewayId)
    {
        $this->gatewayId = $gatewayId;

        return $this;
    }

    /**
     * Get gatewayId.
     *
     * @return string
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }

    /**
     * Set gatewayStatusId.
     *
     * @param string $gatewayStatusId
     *
     * @return LogSms
     */
    public function setGatewayStatusId($gatewayStatusId)
    {
        $this->gatewayStatusId = $gatewayStatusId;

        return $this;
    }

    /**
     * Get gatewayStatusId.
     *
     * @return string
     */
    public function getGatewayStatusId()
    {
        return $this->gatewayStatusId;
    }

    /**
     * Set gatewayStatusText.
     *
     * @param string $gatewayStatusText
     *
     * @return LogSms
     */
    public function setGatewayStatusText($gatewayStatusText)
    {
        $this->gatewayStatusText = $gatewayStatusText;

        return $this;
    }

    /**
     * Get gatewayStatusText.
     *
     * @return string
     */
    public function getGatewayStatusText()
    {
        return $this->gatewayStatusText;
    }

    /**
     * Set gatewayStatusUpdatedAt.
     *
     * @param \DateTime|null $gatewayStatusUpdatedAt
     *
     * @return LogSms
     */
    public function setGatewayStatusUpdatedAt($gatewayStatusUpdatedAt = null)
    {
        $this->gatewayStatusUpdatedAt = $gatewayStatusUpdatedAt;

        return $this;
    }

    /**
     * Get gatewayStatusUpdatedAt.
     *
     * @return \DateTime|null
     */
    public function getGatewayStatusUpdatedAt()
    {
        return $this->gatewayStatusUpdatedAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return LogSms
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return LogSms
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return LogSms
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
