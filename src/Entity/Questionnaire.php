<?php

namespace App\Entity;

/**
 * Questionnaire
 */
class Questionnaire
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $fullName;

    /**
     * @var string
     */
    private $city;

    /**
     * @var \DateTime|null
     */
    private $birthDate;

    /**
     * @var float
     */
    private $weight;

    /**
     * @var string
     */
    private $complaints;

    /**
     * @var string|null
     */
    private $medicines;

    /**
     * @var string|null
     */
    private $menstrual;

    /**
     * @var string|null
     */
    private $additional;

    /**
     * @var string|null
     */
    private $target;

    /**
     * @var int|null
     */
    private $documentId;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var string|null
     */
    private $muscles;

    /**
     * @var int
     */
    private $age = '0';

    /**
     * @var \DateTime
     */
    private $date = '2018-12-03';

    /**
     * @var string|null
     */
    private $drugs;

    /**
     * @var string|null
     */
    private $abortion;

    /**
     * @var string|null
     */
    private $allergy;

    /**
     * @var float
     */
    private $height = '0';

    /**
     * @var string|null
     */
    private $illnesses;

    /**
     * @var string|null
     */
    private $surgery;

    /**
     * @var string|null
     */
    private $drugsFirst;

    /**
     * @var string|null
     */
    private $drugsSecond;

    /**
     * @var string|null
     */
    private $drugsThird;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return Questionnaire
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set fullName.
     *
     * @param string $fullName
     *
     * @return Questionnaire
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName.
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Questionnaire
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set birthDate.
     *
     * @param \DateTime|null $birthDate
     *
     * @return Questionnaire
     */
    public function setBirthDate($birthDate = null)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate.
     *
     * @return \DateTime|null
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set weight.
     *
     * @param float $weight
     *
     * @return Questionnaire
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight.
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set complaints.
     *
     * @param string $complaints
     *
     * @return Questionnaire
     */
    public function setComplaints($complaints)
    {
        $this->complaints = $complaints;

        return $this;
    }

    /**
     * Get complaints.
     *
     * @return string
     */
    public function getComplaints()
    {
        return $this->complaints;
    }

    /**
     * Set medicines.
     *
     * @param string|null $medicines
     *
     * @return Questionnaire
     */
    public function setMedicines($medicines = null)
    {
        $this->medicines = $medicines;

        return $this;
    }

    /**
     * Get medicines.
     *
     * @return string|null
     */
    public function getMedicines()
    {
        return $this->medicines;
    }

    /**
     * Set menstrual.
     *
     * @param string|null $menstrual
     *
     * @return Questionnaire
     */
    public function setMenstrual($menstrual = null)
    {
        $this->menstrual = $menstrual;

        return $this;
    }

    /**
     * Get menstrual.
     *
     * @return string|null
     */
    public function getMenstrual()
    {
        return $this->menstrual;
    }

    /**
     * Set additional.
     *
     * @param string|null $additional
     *
     * @return Questionnaire
     */
    public function setAdditional($additional = null)
    {
        $this->additional = $additional;

        return $this;
    }

    /**
     * Get additional.
     *
     * @return string|null
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * Set target.
     *
     * @param string|null $target
     *
     * @return Questionnaire
     */
    public function setTarget($target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target.
     *
     * @return string|null
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set documentId.
     *
     * @param int|null $documentId
     *
     * @return Questionnaire
     */
    public function setDocumentId($documentId = null)
    {
        $this->documentId = $documentId;

        return $this;
    }

    /**
     * Get documentId.
     *
     * @return int|null
     */
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Questionnaire
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Questionnaire
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set muscles.
     *
     * @param string|null $muscles
     *
     * @return Questionnaire
     */
    public function setMuscles($muscles = null)
    {
        $this->muscles = $muscles;

        return $this;
    }

    /**
     * Get muscles.
     *
     * @return string|null
     */
    public function getMuscles()
    {
        return $this->muscles;
    }

    /**
     * Set age.
     *
     * @param int $age
     *
     * @return Questionnaire
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age.
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Questionnaire
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set drugs.
     *
     * @param string|null $drugs
     *
     * @return Questionnaire
     */
    public function setDrugs($drugs = null)
    {
        $this->drugs = $drugs;

        return $this;
    }

    /**
     * Get drugs.
     *
     * @return string|null
     */
    public function getDrugs()
    {
        return $this->drugs;
    }

    /**
     * Set abortion.
     *
     * @param string|null $abortion
     *
     * @return Questionnaire
     */
    public function setAbortion($abortion = null)
    {
        $this->abortion = $abortion;

        return $this;
    }

    /**
     * Get abortion.
     *
     * @return string|null
     */
    public function getAbortion()
    {
        return $this->abortion;
    }

    /**
     * Set allergy.
     *
     * @param string|null $allergy
     *
     * @return Questionnaire
     */
    public function setAllergy($allergy = null)
    {
        $this->allergy = $allergy;

        return $this;
    }

    /**
     * Get allergy.
     *
     * @return string|null
     */
    public function getAllergy()
    {
        return $this->allergy;
    }

    /**
     * Set height.
     *
     * @param float $height
     *
     * @return Questionnaire
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height.
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set illnesses.
     *
     * @param string|null $illnesses
     *
     * @return Questionnaire
     */
    public function setIllnesses($illnesses = null)
    {
        $this->illnesses = $illnesses;

        return $this;
    }

    /**
     * Get illnesses.
     *
     * @return string|null
     */
    public function getIllnesses()
    {
        return $this->illnesses;
    }

    /**
     * Set surgery.
     *
     * @param string|null $surgery
     *
     * @return Questionnaire
     */
    public function setSurgery($surgery = null)
    {
        $this->surgery = $surgery;

        return $this;
    }

    /**
     * Get surgery.
     *
     * @return string|null
     */
    public function getSurgery()
    {
        return $this->surgery;
    }

    /**
     * Set drugsFirst.
     *
     * @param string|null $drugsFirst
     *
     * @return Questionnaire
     */
    public function setDrugsFirst($drugsFirst = null)
    {
        $this->drugsFirst = $drugsFirst;

        return $this;
    }

    /**
     * Get drugsFirst.
     *
     * @return string|null
     */
    public function getDrugsFirst()
    {
        return $this->drugsFirst;
    }

    /**
     * Set drugsSecond.
     *
     * @param string|null $drugsSecond
     *
     * @return Questionnaire
     */
    public function setDrugsSecond($drugsSecond = null)
    {
        $this->drugsSecond = $drugsSecond;

        return $this;
    }

    /**
     * Get drugsSecond.
     *
     * @return string|null
     */
    public function getDrugsSecond()
    {
        return $this->drugsSecond;
    }

    /**
     * Set drugsThird.
     *
     * @param string|null $drugsThird
     *
     * @return Questionnaire
     */
    public function setDrugsThird($drugsThird = null)
    {
        $this->drugsThird = $drugsThird;

        return $this;
    }

    /**
     * Get drugsThird.
     *
     * @return string|null
     */
    public function getDrugsThird()
    {
        return $this->drugsThird;
    }
}
