<?php

namespace App\Entity;

/**
 * PushNotificationDevices
 */
class PushNotificationDevices
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var string|null
     */
    private $deviceRegistrationId;

    /**
     * @var string|null
     */
    private $deviceArn;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return PushNotificationDevices
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set deviceRegistrationId.
     *
     * @param string|null $deviceRegistrationId
     *
     * @return PushNotificationDevices
     */
    public function setDeviceRegistrationId($deviceRegistrationId = null)
    {
        $this->deviceRegistrationId = $deviceRegistrationId;

        return $this;
    }

    /**
     * Get deviceRegistrationId.
     *
     * @return string|null
     */
    public function getDeviceRegistrationId()
    {
        return $this->deviceRegistrationId;
    }

    /**
     * Set deviceArn.
     *
     * @param string|null $deviceArn
     *
     * @return PushNotificationDevices
     */
    public function setDeviceArn($deviceArn = null)
    {
        $this->deviceArn = $deviceArn;

        return $this;
    }

    /**
     * Get deviceArn.
     *
     * @return string|null
     */
    public function getDeviceArn()
    {
        return $this->deviceArn;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return PushNotificationDevices
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return PushNotificationDevices
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return PushNotificationDevices
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
