<?php

namespace App\Entity;

/**
 * UsersPersons
 */
class UsersPersons
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var \App\Entity\Persons
     */
    private $person;

    /**
     * @var \App\Entity\Users
     */
    private $user;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return UsersPersons
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return UsersPersons
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set person.
     *
     * @param \App\Entity\Persons|null $person
     *
     * @return UsersPersons
     */
    public function setPerson(\App\Entity\Persons $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person.
     *
     * @return \App\Entity\Persons|null
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set user.
     *
     * @param \App\Entity\Users|null $user
     *
     * @return UsersPersons
     */
    public function setUser(\App\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \App\Entity\Users|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
