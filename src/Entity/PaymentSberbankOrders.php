<?php

namespace App\Entity;

/**
 * PaymentSberbankOrders
 */
class PaymentSberbankOrders
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $innerOrderId = '0';

    /**
     * @var string|null
     */
    private $sberbankOrderId;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set innerOrderId.
     *
     * @param int $innerOrderId
     *
     * @return PaymentSberbankOrders
     */
    public function setInnerOrderId($innerOrderId)
    {
        $this->innerOrderId = $innerOrderId;

        return $this;
    }

    /**
     * Get innerOrderId.
     *
     * @return int
     */
    public function getInnerOrderId()
    {
        return $this->innerOrderId;
    }

    /**
     * Set sberbankOrderId.
     *
     * @param string|null $sberbankOrderId
     *
     * @return PaymentSberbankOrders
     */
    public function setSberbankOrderId($sberbankOrderId = null)
    {
        $this->sberbankOrderId = $sberbankOrderId;

        return $this;
    }

    /**
     * Get sberbankOrderId.
     *
     * @return string|null
     */
    public function getSberbankOrderId()
    {
        return $this->sberbankOrderId;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return PaymentSberbankOrders
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return PaymentSberbankOrders
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
