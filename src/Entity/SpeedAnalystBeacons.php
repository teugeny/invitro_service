<?php

namespace App\Entity;

/**
 * SpeedAnalystBeacons
 */
class SpeedAnalystBeacons
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $hash;

    /**
     * @var float
     */
    private $duration = '0';

    /**
     * @var float
     */
    private $timeStart = '0';

    /**
     * @var float
     */
    private $timeEnd = '0';

    /**
     * @var float
     */
    private $timeStartHash = '0';

    /**
     * @var float
     */
    private $timeEndHash = '0';

    /**
     * @var int
     */
    private $count = '0';

    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $fullUrl;

    /**
     * @var string
     */
    private $params;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hash.
     *
     * @param string|null $hash
     *
     * @return SpeedAnalystBeacons
     */
    public function setHash($hash = null)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash.
     *
     * @return string|null
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set duration.
     *
     * @param float $duration
     *
     * @return SpeedAnalystBeacons
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration.
     *
     * @return float
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set timeStart.
     *
     * @param float $timeStart
     *
     * @return SpeedAnalystBeacons
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart.
     *
     * @return float
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set timeEnd.
     *
     * @param float $timeEnd
     *
     * @return SpeedAnalystBeacons
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * Get timeEnd.
     *
     * @return float
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * Set timeStartHash.
     *
     * @param float $timeStartHash
     *
     * @return SpeedAnalystBeacons
     */
    public function setTimeStartHash($timeStartHash)
    {
        $this->timeStartHash = $timeStartHash;

        return $this;
    }

    /**
     * Get timeStartHash.
     *
     * @return float
     */
    public function getTimeStartHash()
    {
        return $this->timeStartHash;
    }

    /**
     * Set timeEndHash.
     *
     * @param float $timeEndHash
     *
     * @return SpeedAnalystBeacons
     */
    public function setTimeEndHash($timeEndHash)
    {
        $this->timeEndHash = $timeEndHash;

        return $this;
    }

    /**
     * Get timeEndHash.
     *
     * @return float
     */
    public function getTimeEndHash()
    {
        return $this->timeEndHash;
    }

    /**
     * Set count.
     *
     * @param int $count
     *
     * @return SpeedAnalystBeacons
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count.
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set method.
     *
     * @param string $method
     *
     * @return SpeedAnalystBeacons
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method.
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set fullUrl.
     *
     * @param string $fullUrl
     *
     * @return SpeedAnalystBeacons
     */
    public function setFullUrl($fullUrl)
    {
        $this->fullUrl = $fullUrl;

        return $this;
    }

    /**
     * Get fullUrl.
     *
     * @return string
     */
    public function getFullUrl()
    {
        return $this->fullUrl;
    }

    /**
     * Set params.
     *
     * @param string $params
     *
     * @return SpeedAnalystBeacons
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get params.
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return SpeedAnalystBeacons
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return SpeedAnalystBeacons
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
