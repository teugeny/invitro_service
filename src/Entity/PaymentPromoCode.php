<?php

namespace App\Entity;

/**
 * PaymentPromoCode
 */
class PaymentPromoCode
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var int|null
     */
    private $countLimit;

    /**
     * @var \DateTime|null
     */
    private $expiredAt;

    /**
     * @var int|null
     */
    private $amount;

    /**
     * @var string|null
     */
    private $amountType;

    /**
     * @var bool
     */
    private $active = true;

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var int
     */
    private $failsCount = '0';

    /**
     * @var bool
     */
    private $showCoupon = false;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return PaymentPromoCode
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set countLimit.
     *
     * @param int|null $countLimit
     *
     * @return PaymentPromoCode
     */
    public function setCountLimit($countLimit = null)
    {
        $this->countLimit = $countLimit;

        return $this;
    }

    /**
     * Get countLimit.
     *
     * @return int|null
     */
    public function getCountLimit()
    {
        return $this->countLimit;
    }

    /**
     * Set expiredAt.
     *
     * @param \DateTime|null $expiredAt
     *
     * @return PaymentPromoCode
     */
    public function setExpiredAt($expiredAt = null)
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    /**
     * Get expiredAt.
     *
     * @return \DateTime|null
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * Set amount.
     *
     * @param int|null $amount
     *
     * @return PaymentPromoCode
     */
    public function setAmount($amount = null)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return int|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set amountType.
     *
     * @param string|null $amountType
     *
     * @return PaymentPromoCode
     */
    public function setAmountType($amountType = null)
    {
        $this->amountType = $amountType;

        return $this;
    }

    /**
     * Get amountType.
     *
     * @return string|null
     */
    public function getAmountType()
    {
        return $this->amountType;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return PaymentPromoCode
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return PaymentPromoCode
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return PaymentPromoCode
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return PaymentPromoCode
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return PaymentPromoCode
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set failsCount.
     *
     * @param int $failsCount
     *
     * @return PaymentPromoCode
     */
    public function setFailsCount($failsCount)
    {
        $this->failsCount = $failsCount;

        return $this;
    }

    /**
     * Get failsCount.
     *
     * @return int
     */
    public function getFailsCount()
    {
        return $this->failsCount;
    }

    /**
     * Set showCoupon.
     *
     * @param bool $showCoupon
     *
     * @return PaymentPromoCode
     */
    public function setShowCoupon($showCoupon)
    {
        $this->showCoupon = $showCoupon;

        return $this;
    }

    /**
     * Get showCoupon.
     *
     * @return bool
     */
    public function getShowCoupon()
    {
        return $this->showCoupon;
    }
}
