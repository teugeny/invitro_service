<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Users
 */
class Users
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $salt;

    /**
     * @var bool
     */
    private $active = true;

    /**
     * @var bool
     */
    private $deleted = false;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * @var bool
     */
    private $confirmed = false;

    /**
     * @var \DateTime|null
     */
    private $confirmedAt;

    /**
     * @var string|null
     */
    private $confirmationCode;

    /**
     * @var string|null
     */
    private $rememberToken;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var bool
     */
    private $isDoctor = false;

    /**
     * @var string
     */
    private $prof = '';

    /**
     * @var string
     */
    private $specialization = '';

    /**
     * @var bool
     */
    private $fromAdmin = false;

    /**
     * @var string|null
     */
    private $encryptionKey;

    /**
     * @var json|null
     */
    private $parameters;

    /**
     * @var int|null
     */
    private $affiliateUserId;

    /**
     * @var string|null
     */
    private $referalLink;

    /**
     * @var int|null
     */
    private $parentUserId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $role;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->role = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login.
     *
     * @param string $login
     *
     * @return Users
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login.
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone.
     *
     * @param string $phone
     *
     * @return Users
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return Users
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt.
     *
     * @param string $salt
     *
     * @return Users
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt.
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Users
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set deleted.
     *
     * @param bool $deleted
     *
     * @return Users
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted.
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return Users
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set confirmed.
     *
     * @param bool $confirmed
     *
     * @return Users
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * Get confirmed.
     *
     * @return bool
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * Set confirmedAt.
     *
     * @param \DateTime|null $confirmedAt
     *
     * @return Users
     */
    public function setConfirmedAt($confirmedAt = null)
    {
        $this->confirmedAt = $confirmedAt;

        return $this;
    }

    /**
     * Get confirmedAt.
     *
     * @return \DateTime|null
     */
    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    /**
     * Set confirmationCode.
     *
     * @param string|null $confirmationCode
     *
     * @return Users
     */
    public function setConfirmationCode($confirmationCode = null)
    {
        $this->confirmationCode = $confirmationCode;

        return $this;
    }

    /**
     * Get confirmationCode.
     *
     * @return string|null
     */
    public function getConfirmationCode()
    {
        return $this->confirmationCode;
    }

    /**
     * Set rememberToken.
     *
     * @param string|null $rememberToken
     *
     * @return Users
     */
    public function setRememberToken($rememberToken = null)
    {
        $this->rememberToken = $rememberToken;

        return $this;
    }

    /**
     * Get rememberToken.
     *
     * @return string|null
     */
    public function getRememberToken()
    {
        return $this->rememberToken;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Users
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Users
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set isDoctor.
     *
     * @param bool $isDoctor
     *
     * @return Users
     */
    public function setIsDoctor($isDoctor)
    {
        $this->isDoctor = $isDoctor;

        return $this;
    }

    /**
     * Get isDoctor.
     *
     * @return bool
     */
    public function getIsDoctor()
    {
        return $this->isDoctor;
    }

    /**
     * Set prof.
     *
     * @param string $prof
     *
     * @return Users
     */
    public function setProf($prof)
    {
        $this->prof = $prof;

        return $this;
    }

    /**
     * Get prof.
     *
     * @return string
     */
    public function getProf()
    {
        return $this->prof;
    }

    /**
     * Set specialization.
     *
     * @param string $specialization
     *
     * @return Users
     */
    public function setSpecialization($specialization)
    {
        $this->specialization = $specialization;

        return $this;
    }

    /**
     * Get specialization.
     *
     * @return string
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * Set fromAdmin.
     *
     * @param bool $fromAdmin
     *
     * @return Users
     */
    public function setFromAdmin($fromAdmin)
    {
        $this->fromAdmin = $fromAdmin;

        return $this;
    }

    /**
     * Get fromAdmin.
     *
     * @return bool
     */
    public function getFromAdmin()
    {
        return $this->fromAdmin;
    }

    /**
     * Set encryptionKey.
     *
     * @param string|null $encryptionKey
     *
     * @return Users
     */
    public function setEncryptionKey($encryptionKey = null)
    {
        $this->encryptionKey = $encryptionKey;

        return $this;
    }

    /**
     * Get encryptionKey.
     *
     * @return string|null
     */
    public function getEncryptionKey()
    {
        return $this->encryptionKey;
    }

    /**
     * Set parameters.
     *
     * @param json|null $parameters
     *
     * @return Users
     */
    public function setParameters($parameters = null)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Get parameters.
     *
     * @return json|null
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Set affiliateUserId.
     *
     * @param int|null $affiliateUserId
     *
     * @return Users
     */
    public function setAffiliateUserId($affiliateUserId = null)
    {
        $this->affiliateUserId = $affiliateUserId;

        return $this;
    }

    /**
     * Get affiliateUserId.
     *
     * @return int|null
     */
    public function getAffiliateUserId()
    {
        return $this->affiliateUserId;
    }

    /**
     * Set referalLink.
     *
     * @param string|null $referalLink
     *
     * @return Users
     */
    public function setReferalLink($referalLink = null)
    {
        $this->referalLink = $referalLink;

        return $this;
    }

    /**
     * Get referalLink.
     *
     * @return string|null
     */
    public function getReferalLink()
    {
        return $this->referalLink;
    }

    /**
     * Set parentUserId.
     *
     * @param int|null $parentUserId
     *
     * @return Users
     */
    public function setParentUserId($parentUserId = null)
    {
        $this->parentUserId = $parentUserId;

        return $this;
    }

    /**
     * Get parentUserId.
     *
     * @return int|null
     */
    public function getParentUserId()
    {
        return $this->parentUserId;
    }

    /**
     * Add role.
     *
     * @param \App\Entity\Roles $role
     *
     * @return Users
     */
    public function addRole(\App\Entity\Roles $role)
    {
        $this->role[] = $role;

        return $this;
    }

    /**
     * Remove role.
     *
     * @param \App\Entity\Roles $role
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRole(\App\Entity\Roles $role)
    {
        return $this->role->removeElement($role);
    }

    /**
     * Get role.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRole()
    {
        return $this->role;
    }
}
