<?php

namespace App\Entity;

/**
 * Persons
 */
class Persons
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $middleName;

    /**
     * @var \DateTime
     */
    private $dateBirth;

    /**
     * @var string
     */
    private $identityDocumentType;

    /**
     * @var string
     */
    private $identityDocumentSeries;

    /**
     * @var string
     */
    private $identityDocumentNumber;

    /**
     * @var \DateTime
     */
    private $identityDocumentDate;

    /**
     * @var string
     */
    private $identityDocumentOrganization;

    /**
     * @var int
     */
    private $legalAddressId;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return Persons
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Persons
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set middleName.
     *
     * @param string $middleName
     *
     * @return Persons
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName.
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set dateBirth.
     *
     * @param \DateTime $dateBirth
     *
     * @return Persons
     */
    public function setDateBirth($dateBirth)
    {
        $this->dateBirth = $dateBirth;

        return $this;
    }

    /**
     * Get dateBirth.
     *
     * @return \DateTime
     */
    public function getDateBirth()
    {
        return $this->dateBirth;
    }

    /**
     * Set identityDocumentType.
     *
     * @param string $identityDocumentType
     *
     * @return Persons
     */
    public function setIdentityDocumentType($identityDocumentType)
    {
        $this->identityDocumentType = $identityDocumentType;

        return $this;
    }

    /**
     * Get identityDocumentType.
     *
     * @return string
     */
    public function getIdentityDocumentType()
    {
        return $this->identityDocumentType;
    }

    /**
     * Set identityDocumentSeries.
     *
     * @param string $identityDocumentSeries
     *
     * @return Persons
     */
    public function setIdentityDocumentSeries($identityDocumentSeries)
    {
        $this->identityDocumentSeries = $identityDocumentSeries;

        return $this;
    }

    /**
     * Get identityDocumentSeries.
     *
     * @return string
     */
    public function getIdentityDocumentSeries()
    {
        return $this->identityDocumentSeries;
    }

    /**
     * Set identityDocumentNumber.
     *
     * @param string $identityDocumentNumber
     *
     * @return Persons
     */
    public function setIdentityDocumentNumber($identityDocumentNumber)
    {
        $this->identityDocumentNumber = $identityDocumentNumber;

        return $this;
    }

    /**
     * Get identityDocumentNumber.
     *
     * @return string
     */
    public function getIdentityDocumentNumber()
    {
        return $this->identityDocumentNumber;
    }

    /**
     * Set identityDocumentDate.
     *
     * @param \DateTime $identityDocumentDate
     *
     * @return Persons
     */
    public function setIdentityDocumentDate($identityDocumentDate)
    {
        $this->identityDocumentDate = $identityDocumentDate;

        return $this;
    }

    /**
     * Get identityDocumentDate.
     *
     * @return \DateTime
     */
    public function getIdentityDocumentDate()
    {
        return $this->identityDocumentDate;
    }

    /**
     * Set identityDocumentOrganization.
     *
     * @param string $identityDocumentOrganization
     *
     * @return Persons
     */
    public function setIdentityDocumentOrganization($identityDocumentOrganization)
    {
        $this->identityDocumentOrganization = $identityDocumentOrganization;

        return $this;
    }

    /**
     * Get identityDocumentOrganization.
     *
     * @return string
     */
    public function getIdentityDocumentOrganization()
    {
        return $this->identityDocumentOrganization;
    }

    /**
     * Set legalAddressId.
     *
     * @param int $legalAddressId
     *
     * @return Persons
     */
    public function setLegalAddressId($legalAddressId)
    {
        $this->legalAddressId = $legalAddressId;

        return $this;
    }

    /**
     * Get legalAddressId.
     *
     * @return int
     */
    public function getLegalAddressId()
    {
        return $this->legalAddressId;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Persons
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Persons
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
