<?php

namespace App\Entity;

/**
 * UsersAddress
 */
class UsersAddress
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var \App\Entity\Address
     */
    private $address;

    /**
     * @var \App\Entity\Users
     */
    private $user;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return UsersAddress
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return UsersAddress
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set address.
     *
     * @param \App\Entity\Address|null $address
     *
     * @return UsersAddress
     */
    public function setAddress(\App\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return \App\Entity\Address|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set user.
     *
     * @param \App\Entity\Users|null $user
     *
     * @return UsersAddress
     */
    public function setUser(\App\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \App\Entity\Users|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
