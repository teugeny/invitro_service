<?php

namespace App\Entity;

/**
 * Company
 */
class Company
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $fullTitle;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $inn;

    /**
     * @var string
     */
    private $kpp;

    /**
     * @var string
     */
    private $fullNameDirector;

    /**
     * @var string
     */
    private $fullNameManager;

    /**
     * @var string
     */
    private $bankAccount;

    /**
     * @var string
     */
    private $bankBic;

    /**
     * @var string
     */
    private $bankName;

    /**
     * @var string
     */
    private $bankAccountCorrection;

    /**
     * @var int
     */
    private $legalAddressId;

    /**
     * @var int
     */
    private $billingAddressId;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullTitle.
     *
     * @param string $fullTitle
     *
     * @return Company
     */
    public function setFullTitle($fullTitle)
    {
        $this->fullTitle = $fullTitle;

        return $this;
    }

    /**
     * Get fullTitle.
     *
     * @return string
     */
    public function getFullTitle()
    {
        return $this->fullTitle;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Company
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set inn.
     *
     * @param string $inn
     *
     * @return Company
     */
    public function setInn($inn)
    {
        $this->inn = $inn;

        return $this;
    }

    /**
     * Get inn.
     *
     * @return string
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * Set kpp.
     *
     * @param string $kpp
     *
     * @return Company
     */
    public function setKpp($kpp)
    {
        $this->kpp = $kpp;

        return $this;
    }

    /**
     * Get kpp.
     *
     * @return string
     */
    public function getKpp()
    {
        return $this->kpp;
    }

    /**
     * Set fullNameDirector.
     *
     * @param string $fullNameDirector
     *
     * @return Company
     */
    public function setFullNameDirector($fullNameDirector)
    {
        $this->fullNameDirector = $fullNameDirector;

        return $this;
    }

    /**
     * Get fullNameDirector.
     *
     * @return string
     */
    public function getFullNameDirector()
    {
        return $this->fullNameDirector;
    }

    /**
     * Set fullNameManager.
     *
     * @param string $fullNameManager
     *
     * @return Company
     */
    public function setFullNameManager($fullNameManager)
    {
        $this->fullNameManager = $fullNameManager;

        return $this;
    }

    /**
     * Get fullNameManager.
     *
     * @return string
     */
    public function getFullNameManager()
    {
        return $this->fullNameManager;
    }

    /**
     * Set bankAccount.
     *
     * @param string $bankAccount
     *
     * @return Company
     */
    public function setBankAccount($bankAccount)
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    /**
     * Get bankAccount.
     *
     * @return string
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * Set bankBic.
     *
     * @param string $bankBic
     *
     * @return Company
     */
    public function setBankBic($bankBic)
    {
        $this->bankBic = $bankBic;

        return $this;
    }

    /**
     * Get bankBic.
     *
     * @return string
     */
    public function getBankBic()
    {
        return $this->bankBic;
    }

    /**
     * Set bankName.
     *
     * @param string $bankName
     *
     * @return Company
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;

        return $this;
    }

    /**
     * Get bankName.
     *
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * Set bankAccountCorrection.
     *
     * @param string $bankAccountCorrection
     *
     * @return Company
     */
    public function setBankAccountCorrection($bankAccountCorrection)
    {
        $this->bankAccountCorrection = $bankAccountCorrection;

        return $this;
    }

    /**
     * Get bankAccountCorrection.
     *
     * @return string
     */
    public function getBankAccountCorrection()
    {
        return $this->bankAccountCorrection;
    }

    /**
     * Set legalAddressId.
     *
     * @param int $legalAddressId
     *
     * @return Company
     */
    public function setLegalAddressId($legalAddressId)
    {
        $this->legalAddressId = $legalAddressId;

        return $this;
    }

    /**
     * Get legalAddressId.
     *
     * @return int
     */
    public function getLegalAddressId()
    {
        return $this->legalAddressId;
    }

    /**
     * Set billingAddressId.
     *
     * @param int $billingAddressId
     *
     * @return Company
     */
    public function setBillingAddressId($billingAddressId)
    {
        $this->billingAddressId = $billingAddressId;

        return $this;
    }

    /**
     * Get billingAddressId.
     *
     * @return int
     */
    public function getBillingAddressId()
    {
        return $this->billingAddressId;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Company
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Company
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Company
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
