<?php

namespace App\Entity;

/**
 * PaymentBalance
 */
class PaymentBalance
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $balance = '0';

    /**
     * @var int
     */
    private $userId;

    /**
     * @var int|null
     */
    private $lastTransactionid;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var int
     */
    private $reserveBalance = '0';


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set balance.
     *
     * @param int $balance
     *
     * @return PaymentBalance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance.
     *
     * @return int
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return PaymentBalance
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set lastTransactionid.
     *
     * @param int|null $lastTransactionid
     *
     * @return PaymentBalance
     */
    public function setLastTransactionid($lastTransactionid = null)
    {
        $this->lastTransactionid = $lastTransactionid;

        return $this;
    }

    /**
     * Get lastTransactionid.
     *
     * @return int|null
     */
    public function getLastTransactionid()
    {
        return $this->lastTransactionid;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return PaymentBalance
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return PaymentBalance
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set reserveBalance.
     *
     * @param int $reserveBalance
     *
     * @return PaymentBalance
     */
    public function setReserveBalance($reserveBalance)
    {
        $this->reserveBalance = $reserveBalance;

        return $this;
    }

    /**
     * Get reserveBalance.
     *
     * @return int
     */
    public function getReserveBalance()
    {
        return $this->reserveBalance;
    }
}
