<?php

namespace App\Entity;

/**
 * ConsultationServiceChanges
 */
class ConsultationServiceChanges
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $consultationId;

    /**
     * @var int
     */
    private $beforeServiceProductId;

    /**
     * @var int
     */
    private $afterServiceProductId;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $userAgent;

    /**
     * @var string
     */
    private $ip;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set consultationId.
     *
     * @param int $consultationId
     *
     * @return ConsultationServiceChanges
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;

        return $this;
    }

    /**
     * Get consultationId.
     *
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * Set beforeServiceProductId.
     *
     * @param int $beforeServiceProductId
     *
     * @return ConsultationServiceChanges
     */
    public function setBeforeServiceProductId($beforeServiceProductId)
    {
        $this->beforeServiceProductId = $beforeServiceProductId;

        return $this;
    }

    /**
     * Get beforeServiceProductId.
     *
     * @return int
     */
    public function getBeforeServiceProductId()
    {
        return $this->beforeServiceProductId;
    }

    /**
     * Set afterServiceProductId.
     *
     * @param int $afterServiceProductId
     *
     * @return ConsultationServiceChanges
     */
    public function setAfterServiceProductId($afterServiceProductId)
    {
        $this->afterServiceProductId = $afterServiceProductId;

        return $this;
    }

    /**
     * Get afterServiceProductId.
     *
     * @return int
     */
    public function getAfterServiceProductId()
    {
        return $this->afterServiceProductId;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return ConsultationServiceChanges
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return ConsultationServiceChanges
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set userAgent.
     *
     * @param string $userAgent
     *
     * @return ConsultationServiceChanges
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Get userAgent.
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Set ip.
     *
     * @param string $ip
     *
     * @return ConsultationServiceChanges
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return ConsultationServiceChanges
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return ConsultationServiceChanges
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return ConsultationServiceChanges
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
