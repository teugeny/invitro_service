<?php

namespace App\Entity;

/**
 * OauthGlobal
 */
class OauthGlobal
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var string
     */
    private $userInternal;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var string|null
     */
    private $profile;

    /**
     * @var string
     */
    private $screenName;

    /**
     * @var string|null
     */
    private $avatar;

    /**
     * @var int|null
     */
    private $expiresStamp;

    /**
     * @var \DateTime|null
     */
    private $expiresDate;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var string|null
     */
    private $devAnswer;

    /**
     * @var string
     */
    private $hash;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $tokenSecret;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return OauthGlobal
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set userInternal.
     *
     * @param string $userInternal
     *
     * @return OauthGlobal
     */
    public function setUserInternal($userInternal)
    {
        $this->userInternal = $userInternal;

        return $this;
    }

    /**
     * Get userInternal.
     *
     * @return string
     */
    public function getUserInternal()
    {
        return $this->userInternal;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return OauthGlobal
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set profile.
     *
     * @param string|null $profile
     *
     * @return OauthGlobal
     */
    public function setProfile($profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return string|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set screenName.
     *
     * @param string $screenName
     *
     * @return OauthGlobal
     */
    public function setScreenName($screenName)
    {
        $this->screenName = $screenName;

        return $this;
    }

    /**
     * Get screenName.
     *
     * @return string
     */
    public function getScreenName()
    {
        return $this->screenName;
    }

    /**
     * Set avatar.
     *
     * @param string|null $avatar
     *
     * @return OauthGlobal
     */
    public function setAvatar($avatar = null)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar.
     *
     * @return string|null
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set expiresStamp.
     *
     * @param int|null $expiresStamp
     *
     * @return OauthGlobal
     */
    public function setExpiresStamp($expiresStamp = null)
    {
        $this->expiresStamp = $expiresStamp;

        return $this;
    }

    /**
     * Get expiresStamp.
     *
     * @return int|null
     */
    public function getExpiresStamp()
    {
        return $this->expiresStamp;
    }

    /**
     * Set expiresDate.
     *
     * @param \DateTime|null $expiresDate
     *
     * @return OauthGlobal
     */
    public function setExpiresDate($expiresDate = null)
    {
        $this->expiresDate = $expiresDate;

        return $this;
    }

    /**
     * Get expiresDate.
     *
     * @return \DateTime|null
     */
    public function getExpiresDate()
    {
        return $this->expiresDate;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return OauthGlobal
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set devAnswer.
     *
     * @param string|null $devAnswer
     *
     * @return OauthGlobal
     */
    public function setDevAnswer($devAnswer = null)
    {
        $this->devAnswer = $devAnswer;

        return $this;
    }

    /**
     * Get devAnswer.
     *
     * @return string|null
     */
    public function getDevAnswer()
    {
        return $this->devAnswer;
    }

    /**
     * Set hash.
     *
     * @param string $hash
     *
     * @return OauthGlobal
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash.
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return OauthGlobal
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set token.
     *
     * @param string $token
     *
     * @return OauthGlobal
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set tokenSecret.
     *
     * @param string $tokenSecret
     *
     * @return OauthGlobal
     */
    public function setTokenSecret($tokenSecret)
    {
        $this->tokenSecret = $tokenSecret;

        return $this;
    }

    /**
     * Get tokenSecret.
     *
     * @return string
     */
    public function getTokenSecret()
    {
        return $this->tokenSecret;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return OauthGlobal
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return OauthGlobal
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
