<?php

namespace App\Entity;

/**
 * PaymentTransactions
 */
class PaymentTransactions
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string|null
     */
    private $payment;

    /**
     * @var string|null
     */
    private $ip;

    /**
     * @var int
     */
    private $price = '0';

    /**
     * @var string|null
     */
    private $paymentDocumentNumber;

    /**
     * @var string|null
     */
    private $paymentData;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var bool
     */
    private $status = false;

    /**
     * @var int|null
     */
    private $promoCodeId;

    /**
     * @var string|null
     */
    private $promoCode;

    /**
     * @var int|null
     */
    private $promoAmount;

    /**
     * @var string|null
     */
    private $promoAmountType;

    /**
     * @var int|null
     */
    private $documentId;

    /**
     * @var int
     */
    private $paymentStatus = '0';

    /**
     * @var string|null
     */
    private $gatewayTransactionId;

    /**
     * @var bool
     */
    private $transactionProcessed = false;

    /**
     * @var string|null
     */
    private $gatewayMessage;

    /**
     * @var int
     */
    private $billBalance = '0';

    /**
     * @var int|null
     */
    private $tmpPrice;

    /**
     * @var bool|null
     */
    private $isAffiliate = false;

    /**
     * @var int
     */
    private $payedAffiliate = '0';


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return PaymentTransactions
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return PaymentTransactions
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return PaymentTransactions
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set payment.
     *
     * @param string|null $payment
     *
     * @return PaymentTransactions
     */
    public function setPayment($payment = null)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment.
     *
     * @return string|null
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set ip.
     *
     * @param string|null $ip
     *
     * @return PaymentTransactions
     */
    public function setIp($ip = null)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set price.
     *
     * @param int $price
     *
     * @return PaymentTransactions
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set paymentDocumentNumber.
     *
     * @param string|null $paymentDocumentNumber
     *
     * @return PaymentTransactions
     */
    public function setPaymentDocumentNumber($paymentDocumentNumber = null)
    {
        $this->paymentDocumentNumber = $paymentDocumentNumber;

        return $this;
    }

    /**
     * Get paymentDocumentNumber.
     *
     * @return string|null
     */
    public function getPaymentDocumentNumber()
    {
        return $this->paymentDocumentNumber;
    }

    /**
     * Set paymentData.
     *
     * @param string|null $paymentData
     *
     * @return PaymentTransactions
     */
    public function setPaymentData($paymentData = null)
    {
        $this->paymentData = $paymentData;

        return $this;
    }

    /**
     * Get paymentData.
     *
     * @return string|null
     */
    public function getPaymentData()
    {
        return $this->paymentData;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return PaymentTransactions
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return PaymentTransactions
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return PaymentTransactions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set promoCodeId.
     *
     * @param int|null $promoCodeId
     *
     * @return PaymentTransactions
     */
    public function setPromoCodeId($promoCodeId = null)
    {
        $this->promoCodeId = $promoCodeId;

        return $this;
    }

    /**
     * Get promoCodeId.
     *
     * @return int|null
     */
    public function getPromoCodeId()
    {
        return $this->promoCodeId;
    }

    /**
     * Set promoCode.
     *
     * @param string|null $promoCode
     *
     * @return PaymentTransactions
     */
    public function setPromoCode($promoCode = null)
    {
        $this->promoCode = $promoCode;

        return $this;
    }

    /**
     * Get promoCode.
     *
     * @return string|null
     */
    public function getPromoCode()
    {
        return $this->promoCode;
    }

    /**
     * Set promoAmount.
     *
     * @param int|null $promoAmount
     *
     * @return PaymentTransactions
     */
    public function setPromoAmount($promoAmount = null)
    {
        $this->promoAmount = $promoAmount;

        return $this;
    }

    /**
     * Get promoAmount.
     *
     * @return int|null
     */
    public function getPromoAmount()
    {
        return $this->promoAmount;
    }

    /**
     * Set promoAmountType.
     *
     * @param string|null $promoAmountType
     *
     * @return PaymentTransactions
     */
    public function setPromoAmountType($promoAmountType = null)
    {
        $this->promoAmountType = $promoAmountType;

        return $this;
    }

    /**
     * Get promoAmountType.
     *
     * @return string|null
     */
    public function getPromoAmountType()
    {
        return $this->promoAmountType;
    }

    /**
     * Set documentId.
     *
     * @param int|null $documentId
     *
     * @return PaymentTransactions
     */
    public function setDocumentId($documentId = null)
    {
        $this->documentId = $documentId;

        return $this;
    }

    /**
     * Get documentId.
     *
     * @return int|null
     */
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     * Set paymentStatus.
     *
     * @param int $paymentStatus
     *
     * @return PaymentTransactions
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * Get paymentStatus.
     *
     * @return int
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * Set gatewayTransactionId.
     *
     * @param string|null $gatewayTransactionId
     *
     * @return PaymentTransactions
     */
    public function setGatewayTransactionId($gatewayTransactionId = null)
    {
        $this->gatewayTransactionId = $gatewayTransactionId;

        return $this;
    }

    /**
     * Get gatewayTransactionId.
     *
     * @return string|null
     */
    public function getGatewayTransactionId()
    {
        return $this->gatewayTransactionId;
    }

    /**
     * Set transactionProcessed.
     *
     * @param bool $transactionProcessed
     *
     * @return PaymentTransactions
     */
    public function setTransactionProcessed($transactionProcessed)
    {
        $this->transactionProcessed = $transactionProcessed;

        return $this;
    }

    /**
     * Get transactionProcessed.
     *
     * @return bool
     */
    public function getTransactionProcessed()
    {
        return $this->transactionProcessed;
    }

    /**
     * Set gatewayMessage.
     *
     * @param string|null $gatewayMessage
     *
     * @return PaymentTransactions
     */
    public function setGatewayMessage($gatewayMessage = null)
    {
        $this->gatewayMessage = $gatewayMessage;

        return $this;
    }

    /**
     * Get gatewayMessage.
     *
     * @return string|null
     */
    public function getGatewayMessage()
    {
        return $this->gatewayMessage;
    }

    /**
     * Set billBalance.
     *
     * @param int $billBalance
     *
     * @return PaymentTransactions
     */
    public function setBillBalance($billBalance)
    {
        $this->billBalance = $billBalance;

        return $this;
    }

    /**
     * Get billBalance.
     *
     * @return int
     */
    public function getBillBalance()
    {
        return $this->billBalance;
    }

    /**
     * Set tmpPrice.
     *
     * @param int|null $tmpPrice
     *
     * @return PaymentTransactions
     */
    public function setTmpPrice($tmpPrice = null)
    {
        $this->tmpPrice = $tmpPrice;

        return $this;
    }

    /**
     * Get tmpPrice.
     *
     * @return int|null
     */
    public function getTmpPrice()
    {
        return $this->tmpPrice;
    }

    /**
     * Set isAffiliate.
     *
     * @param bool|null $isAffiliate
     *
     * @return PaymentTransactions
     */
    public function setIsAffiliate($isAffiliate = null)
    {
        $this->isAffiliate = $isAffiliate;

        return $this;
    }

    /**
     * Get isAffiliate.
     *
     * @return bool|null
     */
    public function getIsAffiliate()
    {
        return $this->isAffiliate;
    }

    /**
     * Set payedAffiliate.
     *
     * @param int $payedAffiliate
     *
     * @return PaymentTransactions
     */
    public function setPayedAffiliate($payedAffiliate)
    {
        $this->payedAffiliate = $payedAffiliate;

        return $this;
    }

    /**
     * Get payedAffiliate.
     *
     * @return int
     */
    public function getPayedAffiliate()
    {
        return $this->payedAffiliate;
    }
}
