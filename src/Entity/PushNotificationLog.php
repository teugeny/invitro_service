<?php

namespace App\Entity;

/**
 * PushNotificationLog
 */
class PushNotificationLog
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var string|null
     */
    private $deviceRegistrationId;

    /**
     * @var string|null
     */
    private $deviceArn;

    /**
     * @var string|null
     */
    private $subject;

    /**
     * @var string|null
     */
    private $text;

    /**
     * @var string|null
     */
    private $payload;

    /**
     * @var string|null
     */
    private $answer;

    /**
     * @var string|null
     */
    private $error;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return PushNotificationLog
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set deviceRegistrationId.
     *
     * @param string|null $deviceRegistrationId
     *
     * @return PushNotificationLog
     */
    public function setDeviceRegistrationId($deviceRegistrationId = null)
    {
        $this->deviceRegistrationId = $deviceRegistrationId;

        return $this;
    }

    /**
     * Get deviceRegistrationId.
     *
     * @return string|null
     */
    public function getDeviceRegistrationId()
    {
        return $this->deviceRegistrationId;
    }

    /**
     * Set deviceArn.
     *
     * @param string|null $deviceArn
     *
     * @return PushNotificationLog
     */
    public function setDeviceArn($deviceArn = null)
    {
        $this->deviceArn = $deviceArn;

        return $this;
    }

    /**
     * Get deviceArn.
     *
     * @return string|null
     */
    public function getDeviceArn()
    {
        return $this->deviceArn;
    }

    /**
     * Set subject.
     *
     * @param string|null $subject
     *
     * @return PushNotificationLog
     */
    public function setSubject($subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject.
     *
     * @return string|null
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set text.
     *
     * @param string|null $text
     *
     * @return PushNotificationLog
     */
    public function setText($text = null)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string|null
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set payload.
     *
     * @param string|null $payload
     *
     * @return PushNotificationLog
     */
    public function setPayload($payload = null)
    {
        $this->payload = $payload;

        return $this;
    }

    /**
     * Get payload.
     *
     * @return string|null
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * Set answer.
     *
     * @param string|null $answer
     *
     * @return PushNotificationLog
     */
    public function setAnswer($answer = null)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer.
     *
     * @return string|null
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set error.
     *
     * @param string|null $error
     *
     * @return PushNotificationLog
     */
    public function setError($error = null)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get error.
     *
     * @return string|null
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return PushNotificationLog
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return PushNotificationLog
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
