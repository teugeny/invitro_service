<?php

namespace App\Entity;

/**
 * Affiliates
 */
class Affiliates
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var int
     */
    private $balance = '0';

    /**
     * @var string|null
     */
    private $usedRefLink;

    /**
     * @var \App\Entity\Users
     */
    private $user;

    /**
     * @var \App\Entity\Users
     */
    private $affiliateUser;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Affiliates
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Affiliates
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set balance.
     *
     * @param int $balance
     *
     * @return Affiliates
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance.
     *
     * @return int
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set usedRefLink.
     *
     * @param string|null $usedRefLink
     *
     * @return Affiliates
     */
    public function setUsedRefLink($usedRefLink = null)
    {
        $this->usedRefLink = $usedRefLink;

        return $this;
    }

    /**
     * Get usedRefLink.
     *
     * @return string|null
     */
    public function getUsedRefLink()
    {
        return $this->usedRefLink;
    }

    /**
     * Set user.
     *
     * @param \App\Entity\Users|null $user
     *
     * @return Affiliates
     */
    public function setUser(\App\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \App\Entity\Users|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set affiliateUser.
     *
     * @param \App\Entity\Users|null $affiliateUser
     *
     * @return Affiliates
     */
    public function setAffiliateUser(\App\Entity\Users $affiliateUser = null)
    {
        $this->affiliateUser = $affiliateUser;

        return $this;
    }

    /**
     * Get affiliateUser.
     *
     * @return \App\Entity\Users|null
     */
    public function getAffiliateUser()
    {
        return $this->affiliateUser;
    }
}
