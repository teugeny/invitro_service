<?php

namespace App\Entity;

/**
 * PaymentInvoices
 */
class PaymentInvoices
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $purpose;

    /**
     * @var int
     */
    private $createdBy;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var string|null
     */
    private $firstName;

    /**
     * @var string|null
     */
    private $lastName;

    /**
     * @var string|null
     */
    private $address;

    /**
     * @var int
     */
    private $status = '0';

    /**
     * @var \DateTime|null
     */
    private $payedAt;

    /**
     * @var \DateTime|null
     */
    private $paymentConfirmedAt;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var string|null
     */
    private $promoCode;

    /**
     * @var int|null
     */
    private $promoCodeId;

    /**
     * @var bool|null
     */
    private $promoCodeSent;

    /**
     * @var \DateTime|null
     */
    private $promoCodeSentAt;

    /**
     * @var string|null
     */
    private $administratorName;

    /**
     * @var int|null
     */
    private $administratorId;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return PaymentInvoices
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set amount.
     *
     * @param float $amount
     *
     * @return PaymentInvoices
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return PaymentInvoices
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set purpose.
     *
     * @param string|null $purpose
     *
     * @return PaymentInvoices
     */
    public function setPurpose($purpose = null)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose.
     *
     * @return string|null
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Set createdBy.
     *
     * @param int $createdBy
     *
     * @return PaymentInvoices
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return PaymentInvoices
     */
    public function setPhone($phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return PaymentInvoices
     */
    public function setFirstName($firstName = null)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string|null $lastName
     *
     * @return PaymentInvoices
     */
    public function setLastName($lastName = null)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set address.
     *
     * @param string|null $address
     *
     * @return PaymentInvoices
     */
    public function setAddress($address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set status.
     *
     * @param int $status
     *
     * @return PaymentInvoices
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set payedAt.
     *
     * @param \DateTime|null $payedAt
     *
     * @return PaymentInvoices
     */
    public function setPayedAt($payedAt = null)
    {
        $this->payedAt = $payedAt;

        return $this;
    }

    /**
     * Get payedAt.
     *
     * @return \DateTime|null
     */
    public function getPayedAt()
    {
        return $this->payedAt;
    }

    /**
     * Set paymentConfirmedAt.
     *
     * @param \DateTime|null $paymentConfirmedAt
     *
     * @return PaymentInvoices
     */
    public function setPaymentConfirmedAt($paymentConfirmedAt = null)
    {
        $this->paymentConfirmedAt = $paymentConfirmedAt;

        return $this;
    }

    /**
     * Get paymentConfirmedAt.
     *
     * @return \DateTime|null
     */
    public function getPaymentConfirmedAt()
    {
        return $this->paymentConfirmedAt;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return PaymentInvoices
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return PaymentInvoices
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return PaymentInvoices
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set promoCode.
     *
     * @param string|null $promoCode
     *
     * @return PaymentInvoices
     */
    public function setPromoCode($promoCode = null)
    {
        $this->promoCode = $promoCode;

        return $this;
    }

    /**
     * Get promoCode.
     *
     * @return string|null
     */
    public function getPromoCode()
    {
        return $this->promoCode;
    }

    /**
     * Set promoCodeId.
     *
     * @param int|null $promoCodeId
     *
     * @return PaymentInvoices
     */
    public function setPromoCodeId($promoCodeId = null)
    {
        $this->promoCodeId = $promoCodeId;

        return $this;
    }

    /**
     * Get promoCodeId.
     *
     * @return int|null
     */
    public function getPromoCodeId()
    {
        return $this->promoCodeId;
    }

    /**
     * Set promoCodeSent.
     *
     * @param bool|null $promoCodeSent
     *
     * @return PaymentInvoices
     */
    public function setPromoCodeSent($promoCodeSent = null)
    {
        $this->promoCodeSent = $promoCodeSent;

        return $this;
    }

    /**
     * Get promoCodeSent.
     *
     * @return bool|null
     */
    public function getPromoCodeSent()
    {
        return $this->promoCodeSent;
    }

    /**
     * Set promoCodeSentAt.
     *
     * @param \DateTime|null $promoCodeSentAt
     *
     * @return PaymentInvoices
     */
    public function setPromoCodeSentAt($promoCodeSentAt = null)
    {
        $this->promoCodeSentAt = $promoCodeSentAt;

        return $this;
    }

    /**
     * Get promoCodeSentAt.
     *
     * @return \DateTime|null
     */
    public function getPromoCodeSentAt()
    {
        return $this->promoCodeSentAt;
    }

    /**
     * Set administratorName.
     *
     * @param string|null $administratorName
     *
     * @return PaymentInvoices
     */
    public function setAdministratorName($administratorName = null)
    {
        $this->administratorName = $administratorName;

        return $this;
    }

    /**
     * Get administratorName.
     *
     * @return string|null
     */
    public function getAdministratorName()
    {
        return $this->administratorName;
    }

    /**
     * Set administratorId.
     *
     * @param int|null $administratorId
     *
     * @return PaymentInvoices
     */
    public function setAdministratorId($administratorId = null)
    {
        $this->administratorId = $administratorId;

        return $this;
    }

    /**
     * Get administratorId.
     *
     * @return int|null
     */
    public function getAdministratorId()
    {
        return $this->administratorId;
    }
}
