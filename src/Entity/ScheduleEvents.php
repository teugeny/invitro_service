<?php

namespace App\Entity;

/**
 * ScheduleEvents
 */
class ScheduleEvents
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $recordId;

    /**
     * @var string|null
     */
    private $recordType;

    /**
     * @var \DateTime|null
     */
    private $startAt;

    /**
     * @var \DateTime|null
     */
    private $endAt;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var int|null
     */
    private $createdBy;

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var bool
     */
    private $marked = false;

    /**
     * @var string|null
     */
    private $color;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recordId.
     *
     * @param int|null $recordId
     *
     * @return ScheduleEvents
     */
    public function setRecordId($recordId = null)
    {
        $this->recordId = $recordId;

        return $this;
    }

    /**
     * Get recordId.
     *
     * @return int|null
     */
    public function getRecordId()
    {
        return $this->recordId;
    }

    /**
     * Set recordType.
     *
     * @param string|null $recordType
     *
     * @return ScheduleEvents
     */
    public function setRecordType($recordType = null)
    {
        $this->recordType = $recordType;

        return $this;
    }

    /**
     * Get recordType.
     *
     * @return string|null
     */
    public function getRecordType()
    {
        return $this->recordType;
    }

    /**
     * Set startAt.
     *
     * @param \DateTime|null $startAt
     *
     * @return ScheduleEvents
     */
    public function setStartAt($startAt = null)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt.
     *
     * @return \DateTime|null
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set endAt.
     *
     * @param \DateTime|null $endAt
     *
     * @return ScheduleEvents
     */
    public function setEndAt($endAt = null)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt.
     *
     * @return \DateTime|null
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return ScheduleEvents
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set createdBy.
     *
     * @param int|null $createdBy
     *
     * @return ScheduleEvents
     */
    public function setCreatedBy($createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return int|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return ScheduleEvents
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return ScheduleEvents
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return ScheduleEvents
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return ScheduleEvents
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return ScheduleEvents
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set marked.
     *
     * @param bool $marked
     *
     * @return ScheduleEvents
     */
    public function setMarked($marked)
    {
        $this->marked = $marked;

        return $this;
    }

    /**
     * Get marked.
     *
     * @return bool
     */
    public function getMarked()
    {
        return $this->marked;
    }

    /**
     * Set color.
     *
     * @param string|null $color
     *
     * @return ScheduleEvents
     */
    public function setColor($color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return string|null
     */
    public function getColor()
    {
        return $this->color;
    }
}
