<?php

namespace App\Command;

use App\Service\DomainService\DomainServiceInterface;
use App\Service\LabService;
use App\Service\Provider\LabProviderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class LoadResults
 * @package App\Command
 */
class LoadInvitroResults extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'magerya:load-lab-results';

    /**
     * @var LabProviderInterface
     */
    protected $provider;

    /**
     * @var LabService
     */
    protected $service;

    /**
     * @var DomainServiceInterface
     */
    protected $domainRepository;

    /**
     * LoadInvitroResults constructor.
     * @param LabProviderInterface $provider
     * @param LabService $service
     */
    public function __construct(LabProviderInterface $provider, LabService $service, DomainServiceInterface $domainRepository)
    {
        $this->provider = $provider;
        $this->service = $service;
        $this->domainRepository = $domainRepository;

        parent::__construct();
    }

    /**
     *
     */
    protected function configure()
    {
        $this->setDescription('Load results from laboratories');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$consultations = $this->domainRepository->getConsultationForInvitro();
        $results = $this->provider->setParams(['uuid' => 'test'])->getResults();
        var_dump($results);
    }
}