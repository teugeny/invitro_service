<?php

namespace App\Service\Facade;


use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractLab
 * @package App\Service\Facade
 */
abstract class AbstractLab
{
    /**
     * @var LoggerInterface
     */
    protected $logger;


    /**
     * @var
     */
    protected $client;

    /**
     * @var
     */
    protected $params;

    /**
     * AbstractLab constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param array $options
     */
    public function configure(array $options)
    {
        $this->params = $options;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getParam($key)
    {
        return $this->params[$key];
    }


    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client): void
    {
        $this->client = $client;
    }
}