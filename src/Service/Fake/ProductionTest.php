<?php

namespace App\Service\Fake;

/**
 * Class ProductionTest
 * @package App\Service\Fake
 */
class ProductionTest
{
    /**
     * @var
     */
    protected $ProductionTestId;

    /**
     * @var
     */
    protected $TestMethodCode;

    /**
     * @var
     */
    protected $RequisitionType;

    /**
     * @var
     */
    protected $Name;

    /**
     * @var
     */
    protected $ResultType;

    /**
     * @return mixed
     */
    public function getProductionTestId()
    {
        return $this->ProductionTestId;
    }

    /**
     * @param mixed $ProductionTestId
     */
    public function setProductionTestId($ProductionTestId): void
    {
        $this->ProductionTestId = $ProductionTestId;
    }

    /**
     * @return mixed
     */
    public function getTestMethodCode()
    {
        return $this->TestMethodCode;
    }

    /**
     * @param mixed $TestMethodCode
     */
    public function setTestMethodCode($TestMethodCode): void
    {
        $this->TestMethodCode = $TestMethodCode;
    }

    /**
     * @return mixed
     */
    public function getRequisitionType()
    {
        return $this->RequisitionType;
    }

    /**
     * @param mixed $RequisitionType
     */
    public function setRequisitionType($RequisitionType): void
    {
        $this->RequisitionType = $RequisitionType;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name): void
    {
        $this->Name = $Name;
    }

    /**
     * @return mixed
     */
    public function getResultType()
    {
        return $this->ResultType;
    }

    /**
     * @param mixed $ResultType
     */
    public function setResultType($ResultType): void
    {
        $this->ResultType = $ResultType;
    }


}