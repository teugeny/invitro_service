<?php

namespace App\Service\DomainService;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;

abstract class AbstractDomainService implements DomainServiceInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository(string $entity)
    {
        return $this->entityManager->getRepository($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function save($entity)
    {
        try {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return $entity;
        } catch (ORMException | \Exception $exception) {
            $this->logger->error($exception->getMessage());
            throw $exception;
        }
    }



    /**
     * {@inheritdoc}
     */
    public function start()
    {
        $this->entityManager->beginTransaction();
    }

    /**
     * {@inheritdoc}
     */
    public function commit()
    {
        $this->entityManager->commit();
    }

    /**
     * {@inheritdoc}
     */
    public function rollback()
    {
        $this->entityManager->rollback();
    }


}