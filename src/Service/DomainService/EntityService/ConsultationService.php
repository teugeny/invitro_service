<?php

namespace App\Service\DomainService\EntityService;

use App\Entity\Consultations;
use App\Service\DomainService\AbstractDomainService;
use Doctrine\ORM\Query\Expr\OrderBy;

/**
 * Class ConsultationService
 * @package App\Service\DomainService\EntityService
 */
class ConsultationService extends AbstractDomainService
{
    /**
     * TODO: После внесений изменений в БД оптимизировать
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function getConsultationForInvitro(): Consultations
    {
        $repository = $this->getRepository(Consultations::class);
        $query = $repository->createQueryBuilder('c');
        $query->select(['*'])
            ->from(Consultations::class, 'c')
            ->where('c.created_at <= :createdAt')
            ->where('c.payed = :isPayed')
            ->where('c.cancel = :isCancel')
            ->where('c.success = :isSuccess')
            ->where('c.success_at IS NULL')
            ->where('c.proposal IS NOT NULL')
            ->setParameters([
                'createdAt' => (new \DateTime('now'))->format('Y-m-d H:i:s'),
                'isPayed' => true,
                'isCancel' => false,
                'isSuccess' => false
            ])
            ->orderBy(new OrderBy('c.created_at', 'DESC'));

        return $query->getQuery()->getSingleResult();
    }
}