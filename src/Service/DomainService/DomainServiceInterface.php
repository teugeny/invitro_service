<?php

namespace App\Service\DomainService;

use App\Service\DomainService\BaseEntityInterface;
use Doctrine\ORM\ORMException;

interface DomainServiceInterface
{
    /**
     * @param string $entity
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository The repository class.
     *
     * @throws ORMException
     * @throws \Exception
     */
    public function getRepository(string $entity);

    /**
     * Save entity as transaction
     *
     * @param object $entity
     * @return object
     */
    public function save($entity);

    /**
     * Start transaction
     */
    public function start();

    /**
     * Finish transaction
     */
    public function commit();


    /**
     * Rollback transaction
     */
    public function rollback();
}