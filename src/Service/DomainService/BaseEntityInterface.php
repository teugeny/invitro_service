<?php
/**
 * Created by PhpStorm.
 * User: eleshanu
 * Date: 18.09.18
 * Time: 19:16
 */

namespace App\Service\DomainService;


interface BaseEntityInterface
{
    /**
     * Get id.
     *
     * @return int
     */
    public function getId();
}