<?php

namespace App\Service\Provider;

/**
 * Interface LabProviderInterface
 * @package App\Service\Provider
 */
interface LabProviderInterface
{
    /**
     * @return mixed
     */
    public function getResults();

    /**
     * @param array $params
     * @return mixed
     */
    public function setParams(array $params);
}