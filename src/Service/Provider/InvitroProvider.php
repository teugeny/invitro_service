<?php

namespace App\Service\Provider;

use App\Service\Facade\AbstractLab;
use App\Service\Fake\ProductionTest;

/**
 * Class InvitroProvider
 * @package App\Service\Provider
 */
class InvitroProvider extends AbstractLab implements LabProviderInterface
{
    /**
     * @return mixed
     */
    public function getResults()
    {
        //TODO Заменить в последствии на запрос к сервису
        return $this->getFackeResponseObject();
    }

    /**
     * @param array $params
     * @return LabProviderInterface
     */
    public function setParams(array $params)
    {
        $this->configure($params);

        return $this;
    }

    /**
     * Метод отдающий фейковые данные.. После окончательной интеграции удалить
     * @return ProductionTest
     */
    private function getFackeResponseObject()
    {
        $response = new ProductionTest();
        $response->setProductionTestId('d7116146-d73b-4a44-ae3c-f5e0da2e34c6');
        $response->setTestMethodCode('ЭФ-ЗАКЛ');
        $response->setRequisitionType('Общая');
        $response->setName('Заключение');
        $response->setResultType('Количественный');

        return $response;
    }
}