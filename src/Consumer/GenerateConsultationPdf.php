<?php

namespace App\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class GenerateConsultationPdf
 * @package App\Consumer
 */
class GenerateConsultationPdf extends AbstractConsumer implements ConsumerInterface
{
    /**
     * @var ProducerInterface
     */
    private $messageProducer;

    /**
     * @param AMQPMessage $msg
     * @return mixed|void
     */
    public function execute(AMQPMessage $msg)
    {
        // TODO: Implement execute() method.
    }

    /**
     * @param ProducerInterface $producer
     */
    public function setMessageProducer(ProducerInterface $producer)
    {
        $this->messageProducer = $producer;
    }
}