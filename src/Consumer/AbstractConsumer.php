<?php

namespace App\Consumer;


use App\Service\DomainService\AbstractDomainService;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Client;

abstract class AbstractConsumer
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var AbstractDomainService
     */
    protected $domainService;


    public function __construct(
        LoggerInterface $logger,
        ?AbstractDomainService $domainService
    )
    {
        $this->logger = $logger;
        $this->domainService = $domainService;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

}